const bot_msgs = {
    start: `Комманды: \n/signUp - регистрация \n/signIn - авторизация\n/getUserInfo - информация о пользователе \n/userUpdate - изменение данных\n/isLoggedToday - заходил ли сегодня пользователь`,
    userInfo: 'Формат: /getUserInfo <id>',
    signIn: 'Формат: /signIn <login> <password>',
    signUp: 'Поля <login> и <password> обязательны \nФормат: /signUp \n<login> \n<password> \n<firstName> \n<middleName> \n<lastName> \n<birthDate>',
    userUpd: 'Поле <id> обязательно, ненужные поля пропускать\nФормат: /userUpdate \nid:<id>\nlogin:<login>:\npassword<password> \nfirstName:<firstName> \nmiddleName:<middleName>\nlastName:<lastName> \nbirthDate:<birthDate>',
    isLogged: 'Формат: /isLoggedToday <id>',
    userNotFound: 'Пользователь не найден',
    incorrectPassword: 'Пароли не совпадают',
    userExists: 'Пользователь с таким логином уже существует!',
    signUpSuccess: 'Регистрация прошла успешно. Ваш id: ',
    userSignedIn: (user) => `userlogin: ${user.userlogin}\npassword: ${user.password}\nfirst name: ${user.firstName}\nmiddle name: ${user.middleName}\nlast name:${user.lastName}\nbirth date: ${user.birthdate}`,
    getUserInfo: (user) =>  `userlogin: ${user.userlogin}\npassword: ${user.password}\nfirst name: ${user.firstName}\nmiddle name: ${user.middleName}\nlast name:${user.lastName}\nbirth date: ${user.birthdate}\nlast login: ${user.lastlogin}`,
    userUpdated: 'Пользователь обновлен',
    loginNotFound: 'Данные отсутствуют',
    userLoggedToday:(id) => `Пользователь с id ${id} заходил сегодня`,
    userNotLoggedToday:(id, lastlogin) => `Пользователь с id ${id} не заходил сегодня. Последняя дата: ${lastlogin}`
}

module.exports = bot_msgs;
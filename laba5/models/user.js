const mongoose = require('mongoose');

let Schema = mongoose.Schema({
    userlogin: {
        type: String,
        unique: true
    },
    password: {
        type: String
    },
    firstName: {
        type: String
    },
    middleName: {
        type: String
    },
    lastName: {
        type: String
    },
    birthdate: {
        type: String
    },
    lastlogin:{
        type: String
    }
})
module.exports = mongoose.model('User', Schema);
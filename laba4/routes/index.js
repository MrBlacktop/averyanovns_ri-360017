var express = require('express');
var router = express.Router();
const { userSignUp, getUserInfo, userSignIn, userUpdate, isLoggedToday } = require('../workers/user_workers');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/sign-up',userSignUp);
router.post('/get-user-info', getUserInfo);
router.post('/sign-in', userSignIn);
router.post('/user-update', userUpdate);
router.post('/is-logged-today', isLoggedToday);

module.exports = router;
